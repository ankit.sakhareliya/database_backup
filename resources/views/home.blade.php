@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <div class="row">
                        <div class="col-md-12">
                        	<a href="{{url('create_backup')}}" class="btn btn-success" role="button">Backup Now!</a>
                        </div>
					</div>
                  
                    <div class="row">
                        <div class="col-md-12">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Name</th>
                                <th>Download</th>
                            </tr>
                            </thead>
                            
                            <tbody>
                                @if(!empty($backup_arr) && count($backup_arr) > 0)
                                    @foreach($backup_arr as $backup)
                                    	@php
                                        	$backup = str_replace("db_backups/", "", $backup);
                                            
                                            
                                            if (preg_match('/db_backup_(.*?).sql/', $backup, $match) == 1) {
                                                
                                                $backup_date = date("Y-m-d H:i:s", strtotime($match[1]));
                                            }

                                        @endphp
                                        <tr>
                                            <td>UTC {{$backup_date}}</td>
                                            <td>{{$backup}}</td>
                                            <td><a href="{{url('download_backup')}}?backup={{$backup}}" class="btn btn-info" role="button">Download</a></td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                        </div>
                    </div>
  
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
