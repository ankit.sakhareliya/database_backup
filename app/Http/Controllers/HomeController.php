<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use Storage;
use Artisan;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$data = array();
		$backup_arr = Storage::disk('local')->files('db_backups');
		rsort($backup_arr);
		
		$data['backup_arr'] = $backup_arr;

        return view('home')->with($data);
    }
	
	public function createBackup()
    {
		Artisan::call('backup:mysql-dump');
		return redirect("/home");
	}
	
	public function downloadBackup()
    {
		
		$backup = Input::get('backup');
		
		return Storage::download("db_backups/".$backup);

	}
	
	
}
